const counterFactory = require('../counterFactory')
let incrementResult = counterFactory().increment()
let decrementResult = counterFactory().decrement()

console.log(incrementResult)
console.log(decrementResult)
