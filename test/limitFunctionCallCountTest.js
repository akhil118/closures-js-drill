const limitFunctionCallCount = require("../limitFunctionCallCount");


const sum  = (a,b) => a + b;
const diff = (a,b) => a - b;
const sumThree = (a,b,c) => a + b + c;
const helloCallback = (x) => (x+" Akhil");

const sum2 = limitFunctionCallCount(sum,2);
const diff2 = limitFunctionCallCount(diff,1);
const sum3 = limitFunctionCallCount(sumThree,1);
const hello = limitFunctionCallCount(helloCallback,1);

console.log(sum2(4,-3))
console.log(diff2(5,4))
console.log(sum3(6,7,8))
console.log(hello("Hello"))

console.log(sum2(4,3))
console.log(diff2(5,4))
console.log(sum3(6,7,8))
console.log(hello("Hai"))
    
