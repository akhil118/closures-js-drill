function counterFactory(counter = 0){
    
    function  increment() {
        return ++counter;
    }
    function decrement(){
        return --counter
    }
    return {
        increment,decrement
    }
}

//  counterFactory().increment()
//  counterFactory().decrement()


 module.exports = counterFactory;
