function limitFunctionCallCount(cb,n){
    let count = 0;

    function calc(...x){
        count++

        if(isNaN(n) == true || cb === undefined || typeof(cb) !== "function"){
            return "This is not defined"

        } else {
            
            if (count <= n){
                    return cb(...x);
                }
            else{
                    return "Callback already called " + n + "-times";
                }
        }

      
    }
    return calc;
}



module.exports = limitFunctionCallCount;
